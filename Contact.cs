﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ContactBookApp
{
    public class Contact
    {
        [Required] 
        public string Name { get; set; }
        [Required]
        [StringLength(100)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        public static Dictionary<string, string> contactsBook { get; private set; }

        public bool IsValid { get; set; }

        public override string ToString()
        {
            return "Name:" + Name + "," + "PhoneNumber:" + PhoneNumber;
        }

        public Contact(string name, string phoneNumber)
        {
            Name = name;
            PhoneNumber = phoneNumber;
        }


        public static Contact GetContact()
        {
            Console.Write("name: ");
            var name = Console.ReadLine();
            var match = Regex.Match(name, @"([A-Za-z]+)");
            
            if(match.Success)
            {
                Console.Write("PhoneNumber: ");
                var phoneNumber = Console.ReadLine();

                var contact = new Contact(name, phoneNumber);

                Validation.CheckNumber(phoneNumber);
                return (contact);
            }
            Console.WriteLine("Please, type Add, Remove, or List to  manage your contacts.");
            return null;






        }

        //public static void Add(List<Contact> contactsBook, Contact contact, JsonFile jsonFile)
        public static void Add(Dictionary<string,string> contactsBook, Contact contact, JsonFile jsonFile, bool isValid)
        {
            try
            {
                if (!isValid)
                {
                    Console.WriteLine("The phone number entered is not in a valid format.");
                }
                else if (contactsBook.Count > 0)
                {
                    contactsBook.Add(contact.PhoneNumber, contact.Name);
                    jsonFile.WriteFile(contactsBook);

                }
                //else
                //{
                //    if (contactsBook.ContainsKey(contact.PhoneNumber))
                //    {
                //        Console.WriteLine("This phone number already exist.");

                //    }

                //}
            }
            catch (Exception e)
            {
                Console.WriteLine("The contact already exists int the Contacts Book. It won't be added again to your file.");
                
            }

        }

        public static void List(Dictionary<string, string>contactsBook)
        {
            //LoadJsonFile();
            var listToPrint = contactsBook.Select(c => $"Name: {c.Value}, Phone number: {c.Key}").ToList();
            for (int i = 0; i < listToPrint.Count; i++)
            {
                Console.WriteLine((i + 1) + listToPrint[i]);

            }
        }

        public static void ManageInput(Constants constants, Dictionary<string, string> contactsBook, JsonFile jsonFile, bool isValid)
        {
            switch (constants)
            {
                case Constants.Add:
                    var contact = Contact.GetContact();
                    Console.WriteLine("You typed Add!!!");
                    Contact.Add(contactsBook, contact, jsonFile, isValid);
                    break;
                case Constants.Remove:
                    //newContact.Remove();
                    Console.WriteLine("You typed Remove");
                    break;
                case Constants.List:
                    Contact.List(contactsBook);
                    Console.WriteLine("\n\nYou typed List");
                    break;
            }
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace ContactBookApp
{
    class Validation
    {
        public string PhoneNumber { get; }
        public string MatchPhoneNumberPattern { get; set; }
        public bool IsValid { get; set; }

        public static bool CheckNumber(string phoneNumber)
        {
            string MatchPhoneNumberPattern = "^[0-9]+$";
            try
            {
                if (phoneNumber != null)
                {
                    return Regex.IsMatch(phoneNumber, MatchPhoneNumberPattern);
                }

                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("The phone number format isn't valid.");
                throw;
            }
            
        }
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ContactBookApp
{
    public class JsonFile
    {
        public void WriteFile(Dictionary<string, string> contactsBook)
        {
            using (StreamWriter writer = new StreamWriter("ContactBook.json", true))
            {
                try
                {
                    if (contactsBook.Count > 0)
                    {
                        var contactToWrite = contactsBook.LastOrDefault();
                        var output = JsonConvert.SerializeObject(contactToWrite);
                        writer.WriteLine(output.ToString());
                    }

                }
                catch (JsonWriterException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

        }

        public void ReadFile()
        {
            using (StreamReader reader = new StreamReader("ContactBook.json", Encoding.Unicode))
            {
                while (true)
                {
                    try
                    {
                        var output = reader.ReadToEnd();
                        var contactsDeserialized = JsonConvert.DeserializeObject<Dictionary<string, string>>(output);
                        foreach (var contact  in contactsDeserialized)
                        {
                            Console.WriteLine(contact.ToString());
                        }

                        string line = reader.ReadLine();
                        if (line == null)
                        {
                            break;
                        }
                        Console.WriteLine(line.ToString());
                        Console.WriteLine(output);
                    }
                    catch (JsonWriterException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                   
            }
        }
    }
}

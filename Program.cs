﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Xsl;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ContactBookApp
{
    public class ContactBookApp
    {
        static void Main(string[] args)
        {
            Console.WriteLine(("Contact Book Console").ToUpper());
            var jsonFile = new JsonFile();
            var isValid = true;
            //jsonFile.ReadFile();

            Dictionary<string, string> contactsBook = new Dictionary<string, string>()
            {
                {"00000000", "Jose"},
                {"11111111", "Maria"},
                {"33333333", "Helena"}
            };

            while (true)
            {
                Console.WriteLine(("\n\n---- Please, type Add, Remove, or List to  manage your contacts. ---- \n\n"));

                var input = Console.ReadLine();
                Constants constants = (Constants)Enum.Parse(typeof(Constants), input);

                Contact.ManageInput(constants, contactsBook, jsonFile, isValid);
                Console.ReadKey();
            }
            //var contactsBook = new List<Contact>();


            //I need here go through the rows in json, not in the list..
            //cause the list is empty when we start... so I have to feed it in with the json.
            //Contact.Add(contactsBook, jsonFile);
            //Contact.List(contactsBook);


        }
    }
}
